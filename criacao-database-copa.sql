CREATE TABLE uniforme (
	codigo			int primary key
      , cor_primaria		varchar(50)
      , cor_secundaria		varchar(50)      
);

CREATE TABLE grupo (
	letra			char(1) primary key
      , ano			int
);

CREATE TABLE jogador (
        cpf			VARCHAR(12) primary key
      , nome			VARCHAR(100)
      , sobrenome		VARCHAR(100)
      , data_nascimento		DATE
      , numero_camisa		INT
      , pais			VARCHAR(80) 
);

CREATE TABLE selecao (
	 pais			VARCHAR(80) primary key
       , hino			VARCHAR(80)
       , codigo_uniforme	int
       , pontos			int
       , letra_grupo		char(1)
);

CREATE TABLE partida (
	 local			VARCHAR(50)
       , data			DATE
       , hora			TIME
       , placar			VARCHAR(20)
       , selecao1		VARCHAR(80)
       , selecao2		VARCHAR(80)
       , PRIMARY KEY (local, data, hora) 
);



/* FOREIGN KEYS */

-- JOGADOR
ALTER TABLE jogador ADD CONSTRAINT fk_jogador_selecao FOREIGN KEY ( pais ) REFERENCES selecao ( pais );

-- SELECAO
ALTER TABLE selecao ADD CONSTRAINT fk_selecao_uniforme FOREIGN KEY ( codigo_uniforme ) REFERENCES uniforme ( codigo );

ALTER TABLE selecao ADD CONSTRAINT fk_selecao_grupo FOREIGN KEY ( letra_grupo ) REFERENCES grupo ( letra );

-- PARTIDA
ALTER TABLE partida ADD CONSTRAINT fk_partida_selecao FOREIGN KEY ( selecao1 ) REFERENCES selecao ( pais );